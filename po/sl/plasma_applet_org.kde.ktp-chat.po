# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 20:16+0100\n"
"PO-Revision-Date: 2016-10-31 20:23+0100\n"
"Last-Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 1.5\n"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:69
#, kde-format
msgid "Pin contact"
msgstr "Pripni stik"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:81
#, kde-format
msgid "Move conversation into a window"
msgstr "Premakni pogovor v okno"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:88
#, kde-format
msgid "Close conversation"
msgstr "Zapri pogovor"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:165
#, kde-format
msgid "Copy Link"
msgstr "Kopiraj povezavo"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:176
#, kde-format
msgid "Copy Text"
msgstr "Kopiraj besedilo"

#: org.kde.ktp-chat/contents/ui/ChatWidget.qml:194
#, kde-format
msgid "Chat is not connected. You cannot send messages at this time"
msgstr "Klepet ni povezan, zato trenutno ni mogoče pošiljati sporočil"

#: org.kde.ktp-chat/contents/ui/main.qml:34
#, kde-format
msgid "Close All Conversations"
msgstr "Zapri vse pogovore"
